package hw2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import clojure.string__init;
import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;


public class RatingsProducer {
	private static final Logger LOG = Logger.getLogger(RatingsProducer.class);
	public static final String TOPIC = "all";
	
	public static void main(String[] args) throws IOException {
		if (args.length != 2) 
        {
            
            System.out.println("Usage: TruckEventsProducer <broker list> <zookeeper>");
            System.exit(-1);
        }
        
        Properties props = new Properties();
        props.put("metadata.broker.list", args[0]);
        props.put("zk.connect", args[1]);
        props.put("serializer.class", "kafka.serializer.StringEncoder");
        props.put("request.required.acks", "1");

        ProducerConfig config = new ProducerConfig(props);

        Producer<String, String> producer = new Producer<String, String>(config);
        String filePath = "/usr/db/ml-1m/ratings.dat";
        File movieLens = new File(filePath);
        BufferedReader bufferedReader = null;
        try {
        	bufferedReader = new BufferedReader(new FileReader(movieLens));
        	String line = null;
        	while ((line = bufferedReader.readLine()) != null) {
        		KeyedMessage<String, String> data = new KeyedMessage<String, String>(TOPIC, line);
                LOG.info("Sending message: " + line);
        		producer.send(data);
        	}
        } finally {
        	if (bufferedReader != null) {
        		bufferedReader.close();
        	}
        }
        
        producer.close();
	}
}
