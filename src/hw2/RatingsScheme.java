package hw2;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.log4j.Logger;

import backtype.storm.spout.Scheme;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

public class RatingsScheme implements Scheme {
	private static final Logger LOG = Logger.getLogger(RatingsScheme.class);
	public static final String c_Field_userId = "userId";
	public static final String c_Field_MovieId = "movieId";
	public static final String c_Field_Rating = "rating";
	
	@Override
	public List<Object> deserialize(byte[] bytes) {
		try {
			String rating = new String(bytes, "UTF-8");
			String[] pieces = rating.split("::");
			
			String userId = pieces[0];
			String movieId = pieces[1];
			String actualRating = pieces[2];
			return new Values(cleanup(userId), cleanup(movieId), cleanup(actualRating));			
		} catch (UnsupportedEncodingException e) {
            LOG.error(e);
            throw new RuntimeException(e);
		}
	}

	@Override
	public Fields getOutputFields() {
		return new Fields(c_Field_userId, c_Field_MovieId, c_Field_Rating);
	}
	
	private String cleanup(String str)
    {
        if (str != null)
        {
            return str.trim().replace("\n", "").replace("\t", "");
        } 
        else
        {
            return str;
        }        
    }
}
