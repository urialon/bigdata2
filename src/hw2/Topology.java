package hw2;

import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import storm.kafka.BrokerHosts;
import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;
import storm.kafka.ZkHosts;
import backtype.storm.Config;
import backtype.storm.StormSubmitter;

import java.util.Properties;

import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Topology {
	private static final Logger LOG = LoggerFactory.getLogger(Topology.class);
	protected Properties topologyConfig;
	private static final String KAFKA_SPOUT_ID = "kafkaSpout";
	private static final String RATING_BOLT_ID = "ratingBolt";
	private static final String SCORE_BOLT_ID = "scoreBolt";
	
	private static final int c_ParallelismLevel = 1;
	
	public Topology() {
		topologyConfig = new Properties();
	}
	
	private void constructKafkaSpout(TopologyBuilder builder) {
		BrokerHosts hosts = new ZkHosts("localhost");
        String topic = RatingsProducer.TOPIC;
        String zkRoot = "/" + topic;
        String consumerGroupId = "StormSpout";

        SpoutConfig spoutConfig = new SpoutConfig(hosts, topic, zkRoot, consumerGroupId);

        spoutConfig.scheme = new SchemeAsMultiScheme(new RatingsScheme());
        spoutConfig.ignoreZkOffsets = true;
        KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);
        builder.setSpout(KAFKA_SPOUT_ID, kafkaSpout);
	}
	
	private void constructRatingBolt(TopologyBuilder builder) {
		RatingBolt ratingBolt = new RatingBolt();
		builder.setBolt(RATING_BOLT_ID, ratingBolt, c_ParallelismLevel).fieldsGrouping(KAFKA_SPOUT_ID, new Fields(RatingsScheme.c_Field_userId));
	}
	
	private void constructScoreBolt(TopologyBuilder builder) {
		ScoreBolt scoreBolt = new ScoreBolt();
		builder.setBolt(SCORE_BOLT_ID, scoreBolt, 1).globalGrouping(RATING_BOLT_ID);
			}
	
	private void buildAndSubmit() throws Exception {
        TopologyBuilder builder = new TopologyBuilder();
        constructKafkaSpout(builder);
        constructRatingBolt(builder);
        constructScoreBolt(builder);
        
        Config conf = new Config();
	conf.setDebug(true);
        
        StormSubmitter.submitTopology("ratings-topology", conf, builder.createTopology());
    }

    public static void main(String[] str) throws Exception
    {
    	HConnection connection = null;
    	Admin admin = null;
    	try 
        {
            connection = HConnectionManager.createConnection(HBaseConfiguration.create());
            admin = connection.getAdmin();
            if (admin.tableExists(TableName.valueOf(RatingBolt.c_SlidingWindowTableName))) {
            	admin.disableTable(TableName.valueOf(RatingBolt.c_SlidingWindowTableName));
            	admin.deleteTable(TableName.valueOf(RatingBolt.c_SlidingWindowTableName));
            }       	
            
            HTableDescriptor descriptor = new HTableDescriptor(TableName.valueOf(RatingBolt.c_SlidingWindowTableName));
        	HColumnDescriptor family = new HColumnDescriptor(RatingBolt.c_Users);
        	descriptor.addFamily(family);
        	
        	admin.createTable(descriptor);
        } 
        catch (Exception e) 
        {
            String errMsg = "Error retrievinging connection and access to HBase Tables";
            LOG.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        } finally {
        	if (connection != null) {
        		connection.close();
        	}
        	if (admin != null) {
        		admin.close();
        	}
        }
    	
        //String configFileLocation = "truck_event_topology.properties";
        Topology ratingsTopology = new Topology();
        ratingsTopology.buildAndSubmit();
    }
}
