package hw2;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Tuple;

public class ScoreBolt extends BaseRichBolt{
    private static final Logger LOG = LoggerFactory.getLogger(ScoreBolt.class);
    private static final Integer c_BatchSize = 1000;
    private OutputCollector m_Collector;
    private Double m_Sum = 0.0;
    private Double m_TotalSum = 0.0;
    private int m_ScoreCount = 0;
    private int m_BatchesCount = 0;
    private PrintWriter m_Writer;
    
	@Override
	public void execute(Tuple tuple) {
		Double score = tuple.getDoubleByField(RatingBolt.c_Score);
		m_Sum += score;
		m_TotalSum += score;
		m_ScoreCount++;
				
    	if (m_ScoreCount >= c_BatchSize) {
    		m_BatchesCount++;
    		LOG.info("Batch: " + m_BatchesCount + ", Got score addition: " + m_Sum + ", total score: " + m_TotalSum);
    		
    		m_Writer.println(m_BatchesCount + "," + m_TotalSum);
    		m_ScoreCount = 0;
    		m_Sum = 0.0;
    	}		
    	m_Collector.ack(tuple);
	}

	@Override
	public void prepare(Map arg0, TopologyContext arg1, OutputCollector collector) {
		m_Collector = collector;
		try {
			m_Writer = new PrintWriter("/var/log/storm/scores.txt", "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer arg0) {
				
	}
	
	@Override
	public void cleanup() {
		m_BatchesCount++;
		LOG.info("Batch: " + m_BatchesCount + " (last batch of size " + m_ScoreCount + "), Got score addition: " + m_Sum + ", total score: " + m_TotalSum);
		m_Writer.close();
	}

}
