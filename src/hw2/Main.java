package hw2;

import java.io.IOException;

import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(RatingBolt.class);
    private static final String c_LiftsTableName = "lifts";
    private static final String c_SlidingWindowTableName = "slidingWindow";
    private static final String c_Users = "users";
    private static final String c_TopK = "topk";
    
	public static void main(String[] args) throws IOException {
		Admin m_Admin;
	    HConnection m_Connection;
	    HTableInterface m_LiftsTable;
	    HTableInterface m_SlidingWindowTable;
	    
		try 
        {
            m_Connection = HConnectionManager.createConnection(HBaseConfiguration.create());
            m_LiftsTable = m_Connection.getTable(c_LiftsTableName);	
            m_Admin = m_Connection.getAdmin();
            if (m_Admin.tableExists(TableName.valueOf(c_SlidingWindowTableName))) {
            	LOG.info("Table " + c_LiftsTableName + " already exists, deleting");
            	m_Admin.disableTable(TableName.valueOf(c_SlidingWindowTableName));
            	m_Admin.deleteTable(TableName.valueOf(c_SlidingWindowTableName));
            }
            
        	HTableDescriptor descriptor = new HTableDescriptor(TableName.valueOf(c_SlidingWindowTableName));
        	HColumnDescriptor family = new HColumnDescriptor(c_Users);
        	descriptor.addFamily(family);
        	
        	m_Admin.createTable(descriptor);
            LOG.info("Created table " + c_LiftsTableName + " successfully");
            m_SlidingWindowTable = m_Connection.getTable(c_SlidingWindowTableName);
        } 
        catch (Exception e) 
        {
            String errMsg = "Error retrievinging connection and access to HBase Tables";
            LOG.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
		
		int userId = 3544;
		String rowKey = "(" + userId + ",1)";
		Get get = new Get(Bytes.toBytes(rowKey));
		Result currentUserRow = m_LiftsTable.get(get);
		
		byte[] userValueByte = currentUserRow.getValue(Bytes.toBytes("topk"), Bytes.toBytes("data"));
		String userValueString = Bytes.toString(userValueByte);
		
		LOG.info("Read user rowKey: " + rowKey + ", value: "+ userValueString);
		
		get = new Get(Bytes.toBytes(userId + ""));
		currentUserRow = m_SlidingWindowTable.get(get);
		LOG.info("Sliding Window is empty: " + currentUserRow.isEmpty());
		
		Put toPut = new Put(Bytes.toBytes(userId + ""));
		toPut.add(Bytes.toBytes(c_Users), Bytes.toBytes("data"), Bytes.toBytes(userValueString));
		try {
			m_SlidingWindowTable.put(toPut);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LOG.info("Wrote user rowKey: " + rowKey + ", value: "+ userValueString);
		get = new Get(Bytes.toBytes(userId + ""));
		currentUserRow = m_SlidingWindowTable.get(get);
		userValueByte = currentUserRow.getValue(Bytes.toBytes(c_Users), Bytes.toBytes("data"));
		userValueString = Bytes.toString(userValueByte);
		LOG.info("Sliding Window is empty: " + currentUserRow.isEmpty());
		
		userValueString += "UPDATE data";
		toPut = new Put(Bytes.toBytes(userId + ""));
		toPut.add(Bytes.toBytes(c_Users), Bytes.toBytes("data"), Bytes.toBytes(userValueString));
		try {
			m_SlidingWindowTable.put(toPut);
			LOG.info("Wrote user rowKey: " + rowKey + ", value: "+ userValueString);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
