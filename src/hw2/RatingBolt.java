package hw2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.regionserver.ScanQueryMatcher.MatchCode;
import org.apache.hadoop.hbase.util.Bytes;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class RatingBolt extends BaseRichBolt {
    private static final Logger LOG = LoggerFactory.getLogger(RatingBolt.class);
    public static final String c_LiftsTableName = "lifts";
    public static final String c_SlidingWindowTableName = "slidingWindow";
    public static final String c_Users = "users";
    public static final String c_TopK = "topk";
    public static final String c_Data = "data";
    public static final String c_Score = "score";
    public enum AggregationType { 
    	MAX, SUM
    }
    
    private int m_Counter = 0;
    private double posWeight = 0.5;
    private double negWeight = 0.5;
    private Admin m_Admin;
    private HConnection m_Connection;
    private HTableInterface m_LiftsTable;
    private HTableInterface m_SlidingWindowTable;
    
    OutputCollector m_Collector;
    
	@Override
	public void execute(Tuple tuple) {
		String userId = tuple.getStringByField(RatingsScheme.c_Field_userId);
		String movieId = tuple.getStringByField(RatingsScheme.c_Field_MovieId);
		String rating = tuple.getStringByField(RatingsScheme.c_Field_Rating);
		String tupleString = userId + "," + movieId + "," + rating;
		//LOG.info(tupleString);
		String[] parts = new String[0];
		
		String newSlidingWindow = "";
		Get get = new Get(Bytes.toBytes(userId));
		Result currentUserRow;
		try {
			currentUserRow = m_SlidingWindowTable.get(get);
			String currentSlidingWindow = "";
			if (!currentUserRow.isEmpty()) {
				byte[] currentSlidingWindowByte = currentUserRow.getValue(Bytes.toBytes(c_Users), Bytes.toBytes(c_Data));
				currentSlidingWindow = Bytes.toString(currentSlidingWindowByte);
				//LOG.info("Got current window for user: " + userId + ", value: " + currentSlidingWindow);
				parts = currentSlidingWindow.split(":");
				
			}
			int start = 0;
			if (parts.length >= 5) {
				start = 1;
			} 
			for (int i = start; i < parts.length ; i++) {
				newSlidingWindow += (parts[i] + ":");
			}
			boolean isNeg = Double.parseDouble(rating) <= 2.5;
			newSlidingWindow += "(" + movieId + "," + (isNeg ? -1 : 1) + ")";

			//LOG.info("Updating window for user: " + userId + ", value: " + newSlidingWindow);
			Put slidingWindowToPut = new Put(Bytes.toBytes(userId));
			slidingWindowToPut.add(Bytes.toBytes(c_Users), Bytes.toBytes("data"), Bytes.toBytes(newSlidingWindow));
			m_SlidingWindowTable.put(slidingWindowToPut);
			
			Map<String, Double> posRecommendations = new HashMap<String, Double>();
			Map<String, Double> negRecommendations = new HashMap<String, Double>();
			Map<String, AggregationType> aggregationTypePerMovie = new HashMap<String, AggregationType>();
			// Map<String, Integer> positiveRecommendationCount = new HashMap<String, Integer>();
			// Map<String, Integer> negativeRecommendationCount = new HashMap<String, Integer>();
			for (String seenMovie: parts) {
				// ******************
				String posOrNeg = seenMovie.substring(1, seenMovie.length() - 1).split(",")[1];
				Boolean positiveLift = false;
                Map<String, Double> recommendations = new HashMap<String, Double>();
                double weight = 0;
				if ("1".equals(posOrNeg)) {
					positiveLift = true;
                    recommendations = posRecommendations;
                    weight = posWeight;
				} else if ("-1".equals(posOrNeg)) {
					positiveLift = false;
                    recommendations = negRecommendations;
                    weight = negWeight;
				} else {
					LOG.info("BAD PARSING of posOrNeg: " + posOrNeg + ", seenMovie: " + seenMovie);
					
				}
						
				Get liftGet = new Get(Bytes.toBytes(seenMovie));
				Result currentLift = m_LiftsTable.get(liftGet);
				String[] recommendedLifts = new String[0];
				if (!currentLift.isEmpty()) {
					byte[] currentLiftBytes = currentLift.getValue(Bytes.toBytes(c_TopK), Bytes.toBytes(c_Data));
					String currentLiftString = Bytes.toString(currentLiftBytes);
					recommendedLifts = currentLiftString.split(":");
				}
				for (String singleLift: recommendedLifts) {
					String[] pair = singleLift.substring(1, singleLift.length() - 1).split(",");
					String recommendedMovie = pair[0];
					Double lift = Double.parseDouble(pair[1]);
					
				
					Double alreadyFoundLift = recommendations.get(recommendedMovie);
					
					// Aggregation Logic: Sum all the lifts
					if (alreadyFoundLift != null) {
						//AggregationType aggregationType = aggregationTypePerMovie.get(recommendedMovie);
						//if (aggregationType.equals(AggregationType.SUM)) {
							// Sum
							//lift += alreadyFoundLift;
							// Product
							lift = lift*weight + alreadyFoundLift;
						//}
						//else {
							// max
							//lift = (lift > alreadyFoundLift) ? lift : alreadyFoundLift;
						//}
					} /*else {
						AggregationType newAggregationType = (Math.random() < 0.5) ? AggregationType.MAX : AggregationType.SUM;
						//aggregationTypePerMovie.put(recommendedMovie, newAggregationType);
					}*/
					
					// ********************************
					// Map<String, Integer> countingMap = null;
					// if (positiveLift) {
						// countingMap = positiveRecommendationCount;
					// } else {
						// countingMap = negativeRecommendationCount;
					// }
					// Integer count = countingMap.get(recommendedMovie);
					// if (count == null) {
						// countingMap.put(recommendedMovie, 1);
					// } else {
						// countingMap.put(recommendedMovie, count + 1);
					// }
					// ***********************************
					
					// positiveRecommendationCount.get(recommendedMovie);
					
					recommendations.put(recommendedMovie, lift);
				}
			}
			
			//LOG.info("Recommended movies for user: " + userId + ", seen movie: " + movieId + ", movies: " + recommendations.keySet().toString());
            double recommendedLift = 0;
			Double posRecommendedLift = posRecommendations.get(movieId);
			if (posRecommendedLift != null) {
                recommendedLift += posRecommendedLift;
            }
            Double negRecommendedLift = negRecommendations.get(movieId);
			if (negRecommendedLift != null) {
                recommendedLift += negRecommendedLift;
            }
            
			if (recommendedLift > 0) {
                for (Entry<String, Double> set : posRecommendations.entrySet()) {
                    Double neglift = negRecommendations.get(set.getKey());
        			if (neglift != null) {
        				neglift += set.getValue();
        			} else {
        				neglift = set.getValue();
        			}
        			negRecommendations.put(set.getKey(), neglift);
				}
                ArrayList<Entry<String, Double>> recommendationsList = new ArrayList<Entry<String, Double>>(negRecommendations.entrySet());
                Collections.sort(recommendationsList, new Comparator<Entry<String, Double>>() {
					public int compare(Entry<String, Double> o1, Entry<String, Double> o2) {
						//REVERSE OREDER 
						return Double.compare(o2.getValue(), o1.getValue());
					}
				});
				int rank = findRank(movieId, recommendationsList);

				// //LOG.info("Found recommendation for user: " + userId + ", movie: " + movieId);
				// ArrayList<Double> recommendationsList = new ArrayList<Double>(recommendations.values());
				// Collections.sort(recommendationsList, Collections.reverseOrder());
				// int rank = recommendationsList.indexOf(recommendedLift) + 1;
				///LOG.info("Found rank: " + rank + " of " + recommendedLift + " in: " + recommendationsList);
				
				Double scoreAddition = 1/(new Double(rank));
				if (isNeg) {
					scoreAddition *= -1;
                    if (posRecommendedLift != null) {
                        posWeight = posWeight*0.5;
                    }
                    if (negRecommendedLift != null) {
                        negWeight = negWeight*0.5;
                    }
				} else {
                    if (posRecommendedLift != null) {
                        posWeight = posWeight*2;
                    }
                    if (negRecommendedLift != null) {
                        negWeight = negWeight*2;
                    }
                }
				m_Collector.emit(tuple, new Values(scoreAddition));
				LOG.info("Emitting score addition for user: " + userId + ", rank: " + rank + ", value: " + 
						scoreAddition);
			
			} else {
				m_Collector.emit(tuple, new Values(0.0));
				//LOG.info("MovieId: " + movieId + " was seen but not recommended for user: " + userId + ", score: " + 0);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			//LOG.error(e.getStackTrace().toString());
		}
		m_Counter++;
		m_Collector.ack(tuple);
	}

	private int findRank(String movieId, ArrayList<Entry<String, Double>> recommendationsList) {
		int rank = 1;
		for (Entry<String, Double> entry : recommendationsList) {
			if (entry.getKey().equals(movieId)) {
				break;
			}
			++rank;
		}
		return rank;
	}

	@Override
	public void prepare(Map arg0, TopologyContext arg1, OutputCollector collector) {
		m_Collector = collector;

		try 
        {
            m_Connection = HConnectionManager.createConnection(HBaseConfiguration.create());
            m_LiftsTable = m_Connection.getTable(c_LiftsTableName);	
            m_SlidingWindowTable = m_Connection.getTable(c_SlidingWindowTableName);
        } 
        catch (Exception e) 
        {
            String errMsg = "Error retrievinging connection and access to HBase Tables";
            LOG.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
		
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields(c_Score));
	}
	
	@Override
    public void cleanup() {
		LOG.info("RatingBolt cleanup. Total count: " + m_Counter);
        try 
        {
                m_LiftsTable.close();
                m_SlidingWindowTable.close();
            	m_Admin.deleteTable(TableName.valueOf(c_SlidingWindowTableName));
            	m_Admin.close();
                m_Connection.close();
        } 
        catch (Exception e) 
        {
                LOG.error("Error closing connections", e);
        }
    }
}
